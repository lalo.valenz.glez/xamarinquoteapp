﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteApp.Models
{
    public class Quote : RealmObject
    {
        public string Text { get; set; }
        public string Author { get; set; }
    }
}
