﻿using QuoteApp.Models;
using Realms;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuoteApp.Services
{
    public interface IQuoteApi
    {
        [Get("/quotes")]
        Task<List<Quote>> GetQuotes();
    }
    public class QuoteService
    {
        IQuoteApi api = RestClient.For<IQuoteApi>("https://type.fit/api");
        private Realm realm;

        public QuoteService()
        {
            realm = Realm.GetInstance();
        }
        public Task<List<Quote>> GetQuotes()
        {
            var quotes = api.GetQuotes();
            return quotes;
        }

        public void AddQuote(Quote quote)
        {
            realm.Write(() =>
            {
                realm.Add(quote);
            });
        }

        public List<Quote> GetAllQuotes()
        {
            var quotes = realm.All<Quote>().ToList();
            return quotes;
        }

        public bool IsThereAnyQuote()
        {
            var quotes = realm.All<Quote>().ToList();
            var any = quotes.Any();
            return any;
        }
    }
}
