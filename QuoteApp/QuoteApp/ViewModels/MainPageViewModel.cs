﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using MvvmHelpers;
using QuoteApp.Models;
using QuoteApp.Services;
using Xamarin.Forms;

namespace QuoteApp.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        protected IUserDialogs Dialogs { get; }
        QuoteService quoteService;
        List<Quote> Quotes;
        private Quote _quote;
        public Quote Quote
        {
            get 
            {
                return _quote;
            }
            set
            {
                _quote = value;
                OnPropertyChanged();
            }
        }

        public ICommand GetRandomQuoteCommand { protected set; get; }
        public MainPageViewModel(IUserDialogs userDialogs)
        {
            Dialogs = userDialogs;
            quoteService = new QuoteService();
            if (!quoteService.IsThereAnyQuote())
            {
                var quotes = GetQuotesAsync();
                SavesQuotes(quotes);
            }
            Quotes = quoteService.GetAllQuotes();
            GetRandomQuoteCommand = new Command(GetRandomQuote);
            GetRandomQuote();
        }

        private List<Quote> GetQuotesAsync()
        {
            Dialogs.ShowLoading("loading...");
            var quotes =  quoteService.GetQuotes().GetAwaiter().GetResult();
            Dialogs.HideLoading();
            return quotes;
        }
        private void SavesQuotes(List<Quote> quotes)
        {
            quotes.ForEach(q =>
            {
                quoteService.AddQuote(q);
            });
        }
        public void GetRandomQuote()
        {
            var random = new Random();
            var index = random.Next(Quotes.Count);
            var quote = Quotes[index];
            Quote = quote;
        }
    }
}
